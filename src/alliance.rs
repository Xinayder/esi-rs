extern crate serde_derive;
extern crate serde_json;

#[derive(Debug, Deserialize)]
pub struct Alliance {

    #[serde(default)]
    alliance_id: i32,

    #[serde(default)]
    corporations: Vec<i32>,

    creator_corporation_id: i32,
    creator_id: i32,
    date_founded: String,

    #[serde(default)]
    executor_corporation_id: Option<i32>,

    #[serde(default)]
    faction_id: Option<i32>,

    #[serde(default)]
    icons: Vec<String>,

    name: String,
    ticker: String
}

pub trait TAlliance: Sized {
    fn get_alliances(&self) -> Result<Vec<i32>, String>;
    fn get_alliance_info(&self, alliance_id: i32) -> Result<Alliance, String>;
    fn get_alliance_corporations(&self, alliance: &mut Alliance) -> Result<(), String>;
    fn get_alliance_icons(&self, alliance: &mut Alliance) -> Result<(), String>;
}

impl TAlliance for super::Esi {
    fn get_alliances(&self) -> Result<Vec<i32>, String> {
        let url = "alliances/";

        match self.get(url) {
            Ok(mut response) => {
                let alliances: Vec<i32> = response.json().unwrap();

                Ok(alliances)
            },
            Err(e) => Err(format!("Failed to get list of active alliances: {error}", error=e))
        }
    }

    fn get_alliance_info(&self, alliance_id: i32) -> Result<Alliance, String> {
        let url = &format!("alliances/{id}/", id=alliance_id);

        match self.get(url) {
            Ok(mut response) => {
                let mut alliance: Alliance = response.json().unwrap();
                alliance.alliance_id = alliance_id;

                Ok(alliance)
            },
            Err(e) => Err(format!("Failed to get information for alliance {id}: {error}", id=alliance_id, error=e))
        }
    }

    fn get_alliance_corporations(&self, alliance: &mut Alliance) -> Result<(), String> {
        let url = &format!("alliances/{id}/corporations/", id=alliance.alliance_id);

        match self.get(url) {
            Ok(mut response) => {
                let corporations: Vec<i32> = response.json().unwrap();
                alliance.corporations = corporations;

                Ok(())
            },
            Err(e) => Err(format!("Failed to get corporations for alliance {id}: {error}", id=alliance.alliance_id, error=e))
        }
    }

    fn get_alliance_icons(&self, alliance: &mut Alliance) -> Result<(), String> {
        let url = &format!("alliances/{id}/icons/", id=alliance.alliance_id);

        match self.get(url) {
            Ok(mut response) => {
                let mut icons: Vec<String> = Vec::with_capacity(2);
                let json_icons: serde_json::Value = response.json().unwrap();

                icons.push(String::from(json_icons["px128x128"].as_str().unwrap()));
                icons.push(String::from(json_icons["px64x64"].as_str().unwrap()));

                alliance.icons = icons;

                Ok(())
            },
            Err(e) => Err(format!("Failed to get icons for alliance {id}: {error}", id=alliance.alliance_id, error=e))
        }
    }
}