extern crate serde_derive;
extern crate serde_json;

#[derive(Debug, Deserialize)]
pub struct Corporation {
    #[serde(default)]
    alliance_id: Option<i32>,

    ceo_id: i32,
    creator_id: i32,
    date_founded: String,
    description: String,

    #[serde(default)]
    faction_id: Option<i32>,

    home_station_id: i32,
    member_count: i32,
    name: String,
    shares: i32,
    tax_rate: f32,
    ticker: String,
    url: String,
    war_eligible: bool
}

pub trait TCorporation: Sized {
    fn get_corporation_info(&self, corp_id: i32) -> Result<Corporation, String>;
}

impl TCorporation for super::Esi {
    fn get_corporation_info(&self, corp_id: i32) -> Result<Corporation, String> {
        let url = &format!("corporations/{}/", corp_id);

        match self.get(url) {
            Ok(mut response) => {
                let mut corp: Corporation = response.json().unwrap();

                Ok(corp)
            },
            Err(e) => Err(format!("Failed to get information for corporation {id}: {error}", id=corp_id, error=e))
        }
    }
}