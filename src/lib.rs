#[macro_use]
extern crate serde_derive;

extern crate reqwest;
extern crate serde;
extern crate serde_json;

use std::str::FromStr;

#[derive(Debug)]
pub struct Esi {
    client: reqwest::Client,
    datasource: Datasource,
    api_version: APIVersion,
    api_access_token: Option<String>,
}

#[derive(Debug)]
pub enum Datasource {
    Tranquility,
    Singularity
}

#[derive(Debug)]
pub enum APIVersion {
    Dev,
    Latest,
    Legacy,
}

impl std::str::FromStr for Datasource {
    type Err = String;

    fn from_str(s: &str) -> Result<Datasource, Self::Err> {
        match s {
            "tranquility" => Ok(Datasource::Tranquility),
            "singularity" => Ok(Datasource::Singularity),
            _ => Err(format!("Unknown datasource. Valid values are: tranquility, singularity"))
        }
    }
}

impl ToString for Datasource {
    fn to_string(&self) -> String {
        match self {
            Datasource::Tranquility => String::from("tranquility"),
            Datasource::Singularity => String::from("singularity"),
        }
    }
}

impl Default for Datasource {
    fn default() -> Self { Datasource::Tranquility }
}

impl std::str::FromStr for APIVersion {
    type Err = String;

    fn from_str(s: &str) -> Result<APIVersion, Self::Err> {
        match s {
            "dev" => Ok(APIVersion::Dev),
            "latest" => Ok(APIVersion::Latest),
            "legacy" => Ok(APIVersion::Legacy),
            _ => Err(format!("Unknown API version. Valid values are: dev, latest, legacy"))
        }
    }
}

impl ToString for APIVersion {
    fn to_string(&self) -> String {
        match self {
            APIVersion::Dev => String::from("dev"),
            APIVersion::Latest => String::from("latest"),
            APIVersion::Legacy => String::from("legacy"),
        }
    }
}

impl Default for APIVersion {
    fn default() -> Self { APIVersion::Latest }
}

impl Esi {

    const BASE_URL: &'static str = "https://esi.evetech.net";

    pub fn new(datasource: &str, api_version: &str) -> Esi {
        let user_agent = format!("esirs/{version} ( {homepage} )",
        version=env!("CARGO_PKG_VERSION"), homepage=env!("CARGO_PKG_HOMEPAGE"));

        use reqwest::header;
        let mut headers = header::HeaderMap::new();

        headers.insert(header::USER_AGENT, header::HeaderValue::from_str(&user_agent).unwrap());
        headers.insert(header::ACCEPT, header::HeaderValue::from_static("application/json"));

        let client = reqwest::Client::builder()
            .default_headers(headers)
            .build();

        let _datasource = match Datasource::from_str(datasource) {
            Ok(ds) => ds,
            Err(_) => Datasource::default()
        };

        let _api_version = match APIVersion::from_str(api_version) {
            Ok(av) => av,
            Err(_) => APIVersion::default()
        };

        Esi {
            client: client.unwrap(),
            datasource: _datasource,
            api_version: _api_version,
            api_access_token: None
        }
    }

    pub fn set_access_token(&mut self, access_token: &str) ->  Result<(), String> {
        self.api_access_token = Some(String::from(access_token));

        match self.get_a("verify/") {
            Ok(mut res) => {
                let json: serde_json::Value = res.json().unwrap();
                dbg!(json);
                Ok(())
            },
            Err(err) => Err(err)
        }
    }

    fn request_url(&self, endpoint: &str, method: reqwest::Method, data: Option<&str>, authenticated_request: bool) -> Result<reqwest::Response, String> {
        let url = match endpoint {
            "verify/" => [Self::BASE_URL, endpoint].join("/"),
            _ => [Self::BASE_URL, &self.api_version.to_string(), endpoint].join("/"),
        };

        let _method = method.clone();

        let mut req_builder = self.client.request(method, &url);
        req_builder = req_builder.query(&[("datasource", &self.datasource.to_string())]);

        match _method {
            reqwest::Method::POST => {
                let json_body: serde_json::Value = serde_json::from_str(data.unwrap()).unwrap();
                req_builder = req_builder.json(&json_body);
            },
            _ => {}
        }

        if authenticated_request {
            let access_token = match &self.api_access_token {
                Some(token) => token,
                None => return Err(format!("Error sending request to '{}': tried to request an authorized endpoint without an access token", url))
            };
            req_builder = req_builder.bearer_auth(access_token);
        }

        dbg!(&req_builder);

        let mut response = req_builder.send().unwrap();

        match response.status().as_str() {
            "400" | "401" | "404" | "420" | "500" | "503" | "504" => {
                let json: serde_json::Value = response.json().unwrap();
                let error = json["error"].as_str().unwrap();

                let error_description: Option<&str> = match json.get("error_description") {
                    Some(err) => Some(err.as_str().unwrap()),
                    None => None
                };

                let error_msg = match error_description {
                    Some(desc) => format!("{}: {}", error, desc),
                    None => format!("{}", error),
                };

                return Err(error_msg);
            },
            _ => {}
        };

        println!("status code: {:?}", response.status());
        println!("url: {:?}", response.url());

        Ok(response)
    }

    fn get(&self, url: &str) -> Result<reqwest::Response, String> {
        self.request_url(url, reqwest::Method::GET, None, false)
    }

    fn get_a(&self, url: &str) -> Result<reqwest::Response, String> {
        self.request_url(url, reqwest::Method::GET, None, true)
    }
}

pub mod alliance;